'use strict';

/**
 * @ngdoc overview
 * @name websiteApp
 * @description
 * # websiteApp
 *
 * Main module of the application.
 */
angular
  .module('websiteApp', [
  	'ui.router',
  	'homeModule'
  	])
  .config(function($stateProvider,$urlRouterProvider){
  	$stateProvider
  	   .state('home',{
  	   		url:"/home",
  	   		templateUrl:"views/home.html",
  	   		controller:"myController"
  	   })
  });