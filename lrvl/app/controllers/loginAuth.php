<?php

class loginAuth extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$code = Input::get( 'code' );
        $linkedinService = OAuth::consumer( 'Linkedin' );

        if ( !empty( $code ) ) {

            $token = $linkedinService->requestAccessToken( $code );
            $result = json_decode($linkedinService->request('/people/~:(id,first-name,last-name,headline,location,public-profile-url,email-address,picture-url)?format=json'), true);
			Session::put('id',$result['firstName']);
			Session::put('username',$result['id']);
			Session::put('emailAddress',$result['emailAddress']);

			return Response::json(array("success"=>array($result),"status"=>Session::put('username')));
			/*echo  json_encode($result,JSON_PRETTY_PRINT);
          	die;
           */

        }// if not ask for permission first
        else {
            $url = $linkedinService->getAuthorizationUri(array('state'=>'DCEEFWF45453sdffef424'));
		   //return Redirect::to( (string)$url );
            return Response::json(array("success"=>array("type"=>"unauthorized","data"=>(string)$url),"status"=>Session::put('username')));
           
        }

        
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function login__with__linkedin()
	{
		
		    // get data from input
        $linkedinService = OAuth::consumer( 'Linkedin','http://localhost/lrvl/public/login' );


        if ( !empty( $code ) ) {

            // This was a callback request from linkedin, get the token
            $token = $linkedinService->requestAccessToken( $code );
            // Send a request with it. Please note that XML is the default format.
            $result = json_decode($linkedinService->request('/people/~?format=json'), true);

            // Show some of the resultant data
            echo 'Your linkedin first name is ' . $result['firstName'] . ' and your last name is ' . $result['lastName'];


            //Var_dump
            //display whole array().
            dd($result);

        }// if not ask for permission first
        else {
            // get linkedinService authorization
            $url = $linkedinService->getAuthorizationUri(array('state'=>'DCEEFWF45453sdffef424'));

            // return to linkedin login url
            return Redirect::to( (string)$url );
        }

	}


}
